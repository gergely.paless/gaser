#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "gaser/core/client/client_manager.h"

TEST(ClientManagerTest, All)
{
	std::string_view dbName = "testDB.db";
	int status;
	status = remove(dbName.data());
	gsr::client_manager clientManager(dbName.data());
	
	std::string_view johnSmith = "John Smith";
	std::string_view johnSmithPass = "myPass";
	
	clientManager.register_client(johnSmith.data(), johnSmithPass.data());
	auto user = clientManager.login_client(johnSmith.data(), johnSmithPass.data());
	EXPECT_EQ(user.username(), johnSmith);
	EXPECT_THROW(clientManager.login_client(johnSmith.data(), "password"), std::logic_error);
	EXPECT_THROW(clientManager.login_client("somebody", "password"), std::logic_error);
	
	clientManager.delete_client(johnSmith.data());
	EXPECT_THROW(clientManager.login_client(johnSmith.data(), johnSmithPass.data()), std::logic_error);
	
	clientManager.register_client(johnSmith.data(), johnSmithPass.data());
	EXPECT_THROW(clientManager.register_client(johnSmith.data(), johnSmithPass.data()), std::logic_error);
}