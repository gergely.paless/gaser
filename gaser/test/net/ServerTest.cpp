#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "gaser/core/server/server.h"
#include "gaser/core/server/connection_handler.h"
#include "gaser/core/client/client_handler.h"

#include <exception>
#include <system_error>
#include <string>

class MockConnectionHandler : public gsr::connection_handler
{
public:
	const std::vector<std::shared_ptr<gsr::tcp_connection>>& connections() const override
	{
		return m_Connections;
	}
	
private:
	std::vector<std::shared_ptr<gsr::tcp_connection>> m_Connections;
};

class MockServer : public gsr::server
{
public:
	explicit MockServer(uint16_t port) :
		gsr::server(
				port,
				std::make_unique<MockConnectionHandler>(),
				std::make_unique<gsr::client_handler>(),
				std::make_unique<gsr::read_strategy_factory>()
				)
	{
	
	}
	
	MOCK_METHOD(void, wait_for_incoming_connection, (), (override));
};

using ::testing::Return;
using ::testing::Throw;

static uint16_t testPort = 4444;

TEST(ServerTest, IsRunning)
{
	MockServer mockPongServer(testPort);
	mockPongServer.start();
	ASSERT_TRUE(mockPongServer.is_running());
}

TEST(ServerTest, WaitIncomingConnection)
{
	MockServer mockPongServer(testPort);
	ON_CALL(mockPongServer, wait_for_incoming_connection);
	EXPECT_CALL(mockPongServer, wait_for_incoming_connection).WillOnce(Return());
	
	mockPongServer.start();
	ASSERT_TRUE(mockPongServer.is_running());
}

TEST(ServerTest, WaitIncomingConnection_ThrowsException)
{
	MockServer mockPongServer(testPort);
	ON_CALL(mockPongServer, wait_for_incoming_connection);
	EXPECT_CALL(mockPongServer, wait_for_incoming_connection)
			.Times(1)
			.WillRepeatedly(Throw(std::system_error(std::error_code{}, "some system error occurred")));
	
	EXPECT_THROW(mockPongServer.start(), std::system_error);
	ASSERT_FALSE(mockPongServer.is_running());
}

TEST(ServerTest, GetPort)
{
	MockServer mockPongServer(testPort);
	EXPECT_EQ(mockPongServer.port(), testPort);
}
