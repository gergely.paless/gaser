#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "gaser/net/connection.h"
#include "gaser/net/tcp_connection.h"
#include "gaser/net/udp_connection.h"
#include "gaser/net/read_strategy/get_size_first_read_strategy.h"

#include <vector>
#include <memory>

class MockConnection final : public gsr::connection
{
public:
	using connection::connection;
	
	MOCK_METHOD(void, close, (const std::function<void()>& closedCallback), (override));
	MOCK_METHOD(bool, is_open, (), (const override));
	
	MOCK_METHOD(void, send_async_impl,(const std::function<void()>& doneCallback));
	MOCK_METHOD(void, receive_async_impl, (const std::function<void()>& doneCallback));
};

class MockConnectionReceiveImplemented final : public gsr::connection
{
public:
	using connection::connection;
	
	MOCK_METHOD(void, close, (const std::function<void()>& closedCallback), (override));
	MOCK_METHOD(bool, is_open, (), (const override));
	
	MOCK_METHOD(void, send_async_impl,(const std::function<void()>& doneCallback));
	
	void receive_async_impl(const std::function<void()>& doneCallback) override
	{
		static bool shouldReturn = false;
		if (shouldReturn)
			return;
		shouldReturn = true;
		doneCallback();
	}
};

class MockConnectionSendImplemented final : public gsr::connection
{
public:
	using connection::connection;
	
	MOCK_METHOD(void, close, (const std::function<void()>& closedCallback), (override));
	MOCK_METHOD(bool, is_open, (), (const override));
	
	MOCK_METHOD(void, receive_async_impl, (const std::function<void()>& doneCallback));
	
	void send_async_impl(const std::function<void()>& doneCallback) override
	{
		std::unique_lock<std::mutex> lock(myMutex);
		++sendAsyncCallCount;
		m_Service.post([=](){
			doneCallback();
		});
		
		if (sendAsyncCallCount >= 2)
			cv.notify_all();
	}
	
public:
	size_t sendAsyncCallCount = 0;
	std::condition_variable cv;
	std::mutex myMutex;
	
};

class MockReadStrategy : public gsr::read_strategy
{
public:
	MockReadStrategy(): m_Body(5)
	{
	
	}
	
	asio::mutable_buffer get_buffer() override
	{
		return {m_Body.data(), m_Body.size()};
	}
	
	size_t how_many_to_receive() override
	{
		return 0;
	}
	
	void received_bytes(size_t) override
	{
	
	}
	
	void publish_packet(const std::weak_ptr<gsr::connection>&) override
	{
	
	}
	
private:
	size_t m_IdBuffer;
	std::vector<uint8_t> m_Body;
};

class GaserConnectionTest : public ::testing::Test
{
public:
	GaserConnectionTest() :
		work(std::make_unique<asio::io_service::work>(asioService))
	{
	
	}
	
	void SetUp() override
	{
		serviceThread = std::thread([this]() {
			asioService.run();
		});
	}
	
	void TearDown() override
	{
		work.reset();
		serviceThread.join();
	}
	
	
public:
	asio::io_service asioService;
	std::thread serviceThread;
	std::unique_ptr<asio::io_service::work> work;
};

using ::testing::Return;
using ::testing::Invoke;

TEST(ReadStrategyTest, GetSizeFirstReadStrategy)
{
	auto packetSize = 10;
	
	gsr::get_size_first_read_strategy readStrategy;
	auto countToReceive = readStrategy.how_many_to_receive();
	EXPECT_EQ(countToReceive, sizeof(gsr::packet::size_t));
	
	auto asioBuffer = readStrategy.get_buffer();
	EXPECT_EQ(asioBuffer.size(), countToReceive);
	
	// "reading" into asioBuffer
	auto* size = (gsr::packet::size_t*)asioBuffer.data();
	*size = packetSize;
	readStrategy.received_bytes(sizeof(gsr::packet::size_t));
	
	countToReceive = readStrategy.how_many_to_receive();
	EXPECT_EQ(countToReceive, packetSize);
	
	asioBuffer = readStrategy.get_buffer();
	EXPECT_EQ(asioBuffer.size(), countToReceive);
	
	std::memset(asioBuffer.data(), 0x01, asioBuffer.size());
	readStrategy.received_bytes(asioBuffer.size());
	
	EXPECT_THROW(readStrategy.received_bytes(0), std::logic_error);
	
	// TODO: refactor this
	/*readStrategy.publish_packet(nullptr);
	EXPECT_EQ(readStrategy.how_many_to_receive(), sizeof(gsr::packet::size_t));*/
}

TEST_F(GaserConnectionTest, ConnectionInterface_StartReceiving)
{
	MockConnection myCustomConnection{asioService, std::make_unique<MockReadStrategy>()};
	
	ON_CALL(myCustomConnection, close).WillByDefault(Return());
	ON_CALL(myCustomConnection, send_async_impl).WillByDefault(Return());
	ON_CALL(myCustomConnection, is_open).WillByDefault(Return(true));
	
	ON_CALL(myCustomConnection, receive_async_impl).WillByDefault(Return());
	EXPECT_CALL(myCustomConnection, receive_async_impl)
		.Times(2)
		.WillOnce(Invoke([&](){ myCustomConnection.start_receiving(); }))
		.WillOnce(Return());
	
	myCustomConnection.start_receiving();
}

TEST_F(GaserConnectionTest, ConnectionInterface_StartReceiving_DoneCallback)
{
	MockConnectionReceiveImplemented myCustomConnection{asioService, std::make_unique<MockReadStrategy>()};
	
	ON_CALL(myCustomConnection, close);
	ON_CALL(myCustomConnection, send_async_impl);
	ON_CALL(myCustomConnection, is_open);
		
	myCustomConnection.start_receiving();
}

TEST_F(GaserConnectionTest, ConnectionInterface_Sending)
{
	MockConnectionSendImplemented myCustomConnection{asioService, std::make_unique<MockReadStrategy>()};
	
	ON_CALL(myCustomConnection, close);
	ON_CALL(myCustomConnection, receive_async_impl);
	ON_CALL(myCustomConnection, is_open);
	
	myCustomConnection.send(gsr::packet{{}});
	myCustomConnection.send(gsr::packet{{}});
	
	std::unique_lock<std::mutex> lock{myCustomConnection.myMutex};
	myCustomConnection.cv.wait_for(lock, std::chrono::duration(std::chrono::seconds(2)));
	EXPECT_EQ(myCustomConnection.sendAsyncCallCount, 2);
}
