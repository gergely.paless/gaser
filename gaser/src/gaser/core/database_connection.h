#pragma once

namespace gsr
{
	class database_connection
	{
	public:
		virtual ~database_connection() = default;
		virtual void Execute() = 0;
	};
}
