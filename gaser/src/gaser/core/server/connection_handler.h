#pragma once

#include "asio.hpp"
#include "gaser/net/tcp_connection.h"

#include <system_error>
#include <memory>

namespace gsr
{
	class connection_handler
	{
	public:
		virtual ~connection_handler() = default;
		
		// return true if there is no error during connection
		virtual bool handle_connection_error(const std::error_code& ec, const asio::ip::tcp::socket& newConnection);
		
		// returns true if we should accept the connection
		virtual void on_incoming_connection(const std::shared_ptr<tcp_connection>& socket);
		
		virtual const std::vector<std::shared_ptr<gsr::tcp_connection>>& connections() const = 0;
	};
}
