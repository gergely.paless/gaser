#include "server.h"

#include "gaser/net/read_strategy/get_size_first_read_strategy.h"


namespace gsr
{
	server::server(uint16_t port,
				   std::unique_ptr<connection_handler> connectionHandler,
				   std::unique_ptr<client_handler> clientHandler,
				   std::unique_ptr<read_strategy_factory> readStrategyFactory) :
			m_Service(),
			m_Acceptor(m_Service, asio::ip::tcp::endpoint(asio::ip::tcp::v4(), port)),
			m_ConnectionHandler(std::move(connectionHandler)),
			m_ClientHandler(std::move(clientHandler)),
			m_ReadStrategyFactory(std::move(readStrategyFactory))
	{
	
	}
	
	server::~server()
	{
		if (m_IsRunning)
			server::stop();
	}
	
	void server::start()
	{
		if (m_IsRunning)
			return;
		
		// Issue a task to the asio m_Service - This is important
		// as it will prime the m_Service with "work", and stop it
		// from exiting immediately. Since this is a server, we
		// want it primed ready to handle clients trying to
		// connect.
		wait_for_incoming_connection();
		launch_service_thread();
		
		m_IsRunning = true;
	}
	
	void server::stop()
	{
		// Request the m_Service to close
		m_Service.stop();
		// Tidy up the m_Service thread
		m_ServiceThread.join();
		
		// Inform someone, anybody, if they care...
		m_IsRunning = false;
	}
	
	bool server::is_running() const
	{
		return m_IsRunning;
	}
	
	uint16_t server::port() const
	{
		return m_Acceptor.local_endpoint().port();
	}
	
	void server::wait_for_incoming_connection()
	{
		// Prime m_Service with an instruction to wait until a socket connects. This
		// is the purpose of an "acceptor" object. It will provide a unique socket
		// for each incoming connection attempt
		m_Acceptor.async_accept(
				[this](std::error_code ec, asio::ip::tcp::socket socket) {
					if (m_ConnectionHandler->handle_connection_error(ec, socket))
					{
						m_ConnectionHandler->on_incoming_connection(
								std::make_shared<tcp_connection>(
										std::move(socket),
										m_Service,
										m_ReadStrategyFactory->create_read_strategy()));
					}
					
					// ALWAYS Prime the asio m_Service with more work - again simply wait for another connection...
					wait_for_incoming_connection();
				}
		);
	}
	
	void server::launch_service_thread()
	{
		// Launch the asio m_Service in its own thread
		m_ServiceThread = std::thread([this]() {
			m_Service.run();
		});
	}
}
