#pragma once

#include "gaser/net/connection.h"
#include "gaser/net/tcp_connection.h"
#include "gaser/core/client/client_handler.h"
#include "connection_handler.h"
#include "gaser/net/read_strategy/read_strategy_factory.h"

#include "asio.hpp"

#include <cstdint>
#include <atomic>
#include <functional>
#include <utility>
#include <memory>

namespace gsr
{
	class server
	{
	public:
		server(uint16_t port,
			   std::unique_ptr<connection_handler> connectionHandler,
			   std::unique_ptr<client_handler> clientHandler,
			   std::unique_ptr<read_strategy_factory> readStrategyFactory);
		
		server(const server&) = delete;
		server& operator=(const server&) = delete;
		
		virtual ~server();
		
		// TODO: replace start, stop with run, which blocks, and run the service thread on the main thread
		virtual void start();
		virtual void stop();
		virtual bool is_running() const;
		virtual uint16_t port() const;
		
		const connection_handler& get_connection_handler() const
		{
			return *m_ConnectionHandler;
		}
		
	private:
		// ASYNC - Instruct asio to wait for connection
		virtual void wait_for_incoming_connection(); // TODO: should not be virtual
		void launch_service_thread();
		
	protected:
		asio::io_service m_Service;
		std::thread m_ServiceThread;
		
		// These things need an asio m_Service
		asio::ip::tcp::acceptor m_Acceptor; // Handles new incoming connection attempts...
		
		std::atomic<bool> m_IsRunning = false;
		
		std::unique_ptr<connection_handler> m_ConnectionHandler;
		std::unique_ptr<client_handler> m_ClientHandler;
		std::unique_ptr<read_strategy_factory> m_ReadStrategyFactory;
		
	private:
	
	};
}
