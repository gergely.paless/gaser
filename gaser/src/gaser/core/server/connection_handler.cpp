#include "connection_handler.h"

namespace gsr
{
	bool connection_handler::handle_connection_error(const std::error_code& ec, const asio::ip::tcp::socket& newConnection)
	{
		return !ec;
	}
	
	void connection_handler::on_incoming_connection(const std::shared_ptr<tcp_connection>& socket)
	{
		socket->start_receiving();
	}
}