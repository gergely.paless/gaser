#include "client_manager.h"

namespace gsr
{
	client_manager::client_manager(const std::string& dbName)
	{
		char* errorMessage;
		sqlite3_open(dbName.c_str(), &db);
		int success = sqlite3_exec(
				db,
				"CREATE TABLE IF NOT EXISTS users(username TEXT, password TEXT)",
				execution_callback,
				nullptr,
				&errorMessage);
		if (success != SQLITE_OK)
		{
			std::cout << "Error: " << errorMessage;
		}
		sqlite3_free(errorMessage);
	}
	
	client_manager::~client_manager()
	{
		sqlite3_close(db);
	}
	
	void client_manager::register_client(const std::string& username, const std::string& password)
	{
		if (is_client_registered(username))
		{
			throw std::logic_error("user already registered");
		}
		
		std::string query = fmt::format(R"(INSERT INTO users(username, password) VALUES ("{}", "{}");)", username, password);
		char* errorMessage;
		int success = sqlite3_exec(db, query.c_str(), execution_callback, nullptr, &errorMessage);
		if (success != SQLITE_OK)
		{
			std::cout << "SQLite error: " << errorMessage;
		}
		sqlite3_free(errorMessage);
	}
	
	user client_manager::login_client(const std::string& username, const std::string& password)
	{
		std::string query = fmt::format(R"(SELECT username, password FROM users WHERE username="{}" AND password="{}";)", username, password);
		user c{username};
		std::tuple<std::string, std::string, bool> clientPair = std::make_tuple(username, password, false);
		char* errorMessage;
		int success = sqlite3_exec(db, query.c_str(), sql_client_login_callback, &clientPair, &errorMessage);
		if (success != SQLITE_OK)
		{
			std::cout << "SQLite error: " << errorMessage;
		}
		sqlite3_free(errorMessage);
		
		auto& [uname, passw, logged_in] = clientPair;
		if (!logged_in)
			throw std::logic_error("username or password is incorrect");
			
		return user{username};
	}
	
	void client_manager::delete_client(const std::string& username)
	{
		std::string query = fmt::format(R"(DELETE FROM users WHERE username="{}";)", username);
		char* errorMessage;
		int success = sqlite3_exec(db, query.c_str(), sql_client_delete_callback, nullptr, &errorMessage);
		if (success != SQLITE_OK)
		{
			std::cout << "SQLite error: " << errorMessage;
		}
		sqlite3_free(errorMessage);
	}
	
	bool client_manager::is_client_registered(const std::string& username)
	{
		std::string query = fmt::format(R"(SELECT username FROM users WHERE username="{}")", username);
		// TODO: user std::optional
		std::pair<std::string, bool> resultForUsername = std::make_pair(username, false);
		char* errorMessage;
		int success = sqlite3_exec(db, query.c_str(), sql_client_registered_callback, &resultForUsername, &errorMessage);
		if (success != SQLITE_OK)
		{
			// TODO: should throw
			sqlite3_free(errorMessage);
			return true;
		}
		sqlite3_free(errorMessage);
		return resultForUsername.second;
	}
	
	int client_manager::sql_client_login_callback(void* resultPtr, int count, char** rows, char** columnNames)
	{
		auto& result = *static_cast<std::tuple<std::string, std::string, bool>*>(resultPtr);
		auto& [username, password, success] = result;
		if (rows[0] == username && rows[1] == password)
		{
			success = true;
		}
		return 0;
	}
	
	int client_manager::sql_client_delete_callback(void* resultPtr, int count, char** rows, char** columnNames)
	{
		return 0;
	}
	
	int client_manager::sql_client_registered_callback(void* resultPtr, int count, char** rows, char** columnNames)
	{
		auto& result = *static_cast<std::pair<std::string, bool>*>(resultPtr);
		for (int i = 0; i < count; ++i)
		{
			if (rows[i] == result.first)
			{
				result.second = true;
				return 1;
			}
		}
		return 0;
	}
	
	int client_manager::execution_callback(void* self_in, int count, char** header, char** body)
	{
		auto self = static_cast<client_manager*>(self_in);
		
		std::cout << "count: " << count << std::endl;
		
		for (int i = 0; i < 2; ++i)
		{
			std::cout << header[i] << " ";
		}
		std::cout << std::endl;
		
		for (int i = 0; i < 2; ++i)
		{
			std::cout << body[i] << " ";
		}
		std::cout << std::endl;
		return 0;
	}
}
