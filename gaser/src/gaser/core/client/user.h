#pragma once

#include <string>

namespace gsr
{
	class user
	{
	public:
		explicit user(std::string username) : m_Username(std::move(username))
		{
		
		}
		
		const std::string& username() const
		{
			return m_Username;
		}
		
		bool operator==(const user& other)
		{
			return m_Username == other.m_Username;
		}
		
		bool operator!=(const user& other)
		{
			return !(*this == other);
		}
		
	private:
		std::string m_Username;
	};
}
