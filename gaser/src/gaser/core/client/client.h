#pragma once

#include "user.h"
#include "gaser/net/tcp_connection.h"

#include <memory>

namespace gsr
{
	class client
	{
	public:
		client(user user, std::shared_ptr<tcp_connection> connection) :
				m_User(std::move(user)),
				m_TcpConnection(std::move(connection))
		{
		
		}
		
		client(const client&) = delete;
		client& operator=(const client&) = delete;
		
		virtual ~client() = default;
		
		const user& user_data() const
		{
			return m_User;
		}
		
		const tcp_connection& connection() const
		{
			return *m_TcpConnection;
		}
		
	private:
		user m_User;
		std::shared_ptr<tcp_connection> m_TcpConnection;
	};
}
