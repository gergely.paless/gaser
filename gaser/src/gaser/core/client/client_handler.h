#pragma once

#include "gaser/net/tcp_connection.h"

#include <memory>

namespace gsr
{
	class client_handler
	{
	public:
		virtual ~client_handler() = default;
		
		virtual bool on_client_connect(std::shared_ptr<tcp_connection> incomingConnection);
		virtual void on_client_disconnect(std::shared_ptr<tcp_connection> connection);
		virtual void packet_received(std::shared_ptr<connection> connection, std::shared_ptr<packet> packet);
	};
}
