#include "client_handler.h"

namespace gsr
{
	bool client_handler::on_client_connect(std::shared_ptr<tcp_connection> incomingConnection)
	{
		return true;
	}
	
	void client_handler::on_client_disconnect(std::shared_ptr<tcp_connection> connection)
	{
	
	}
	
	void client_handler::packet_received(std::shared_ptr<connection> connection, std::shared_ptr<packet> packet)
	{
	
	}
}
