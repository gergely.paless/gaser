#pragma once

#include "gaser/utils/utils.h"

#include "sqlite3.h"
#include "user.h"

#include <string>
#include <iostream>

namespace gsr
{
	class client_manager
	{
	public:
		explicit client_manager(const std::string& dbName = gsr::utils::get_database_file_path().data());
		
		virtual ~client_manager();
		
		// just register, does not actually log in the user
		void register_client(const std::string& username, const std::string& password);
		user login_client(const std::string& username, const std::string& password);
		void delete_client(const std::string& username);
		
	private:
		bool is_client_registered(const std::string& username);
		
		static int sql_client_login_callback(void* resultPtr, int count, char** rows, char** columnNames);
		static int sql_client_delete_callback(void* resultPtr, int count, char** rows, char** columnNames);
		static int sql_client_registered_callback(void* resultPtr, int count, char** rows, char** columnNames);
		static int execution_callback(void* self_in, int count, char** header, char** body);
		
	private:
		sqlite3* db{};
	};
	
}
