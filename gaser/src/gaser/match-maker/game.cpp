#include "game.h"

namespace gsr
{
	game::game(std::set<std::shared_ptr<connection>> clients) :
			m_ConnectionsInGame(std::move(clients)),
			m_GameId(s_GameId++),
			m_GameEnded(false)
	{
	
	}
	
	bool game::ended() const
	{
		return m_GameEnded;
	}
}
