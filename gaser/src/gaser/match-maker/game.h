#pragma once

#include "gaser/core/client/client.h"

#include <set>
#include <memory>

namespace gsr
{
	class game
	{
		static inline size_t s_GameId = 1;
		
	public:
		explicit game(std::set<std::shared_ptr<connection>> clients);
	
		virtual void update() = 0;
		
		bool ended() const;
		
		const std::set<std::shared_ptr<connection>>& GetConnectionsInGame() const
		{
			return m_ConnectionsInGame;
		}
		
	protected:
		std::set<std::shared_ptr<connection>> m_ConnectionsInGame;
		size_t m_GameId;
		bool m_GameEnded;
	};
}
