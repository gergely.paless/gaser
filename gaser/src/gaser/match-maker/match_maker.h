#pragma once

#include "game.h"
#include "gaser/core/client/client.h"


#include <memory>
#include <set>

namespace gsr
{
	class match_maker
	{
	public:
		void queue_client(std::shared_ptr<connection> client);
		void dequeue_client(const std::shared_ptr<connection>& client);
		
		// should be called on a dedicated thread
		void run();
		void stop();
		
	protected:
		virtual void try_to_create_game() = 0;
	
	protected:
		std::vector<std::shared_ptr<connection>> m_QueuedConnections;
		std::mutex m_QueuedClientsMutex;
		std::condition_variable m_MatchMakerChanged;
		
		bool m_ShouldStop = false;
	};
}
