#include "match_maker.h"

namespace gsr
{
	
	void match_maker::queue_client(std::shared_ptr<connection> connection)
	{
		std::unique_lock<std::mutex> lock(m_QueuedClientsMutex);
		m_QueuedConnections.emplace_back(std::move(connection));
		m_MatchMakerChanged.notify_all();
	}
	
	void match_maker::dequeue_client(const std::shared_ptr<connection>& connection)
	{
		std::unique_lock<std::mutex> lock(m_QueuedClientsMutex);
		auto erasableConnection = std::find(m_QueuedConnections.begin(), m_QueuedConnections.end(), connection);
		if (erasableConnection == m_QueuedConnections.end())
			return;
		
		m_QueuedConnections.erase(erasableConnection);
		m_MatchMakerChanged.notify_all();
	}
	
	void match_maker::run()
	{
		m_ShouldStop = false;
		
		std::unique_lock<std::mutex> lock(m_QueuedClientsMutex);
		m_MatchMakerChanged.wait(lock);
		while (!m_ShouldStop)
		{
			try_to_create_game();
			m_MatchMakerChanged.wait(lock);
		}
	}
	
	void match_maker::stop()
	{
		m_ShouldStop = true;
		m_MatchMakerChanged.notify_all();
	}
}
