#pragma once

#include "spdlog/spdlog.h"
#include "spdlog/sinks/basic_file_sink.h"
#include "spdlog/sinks/stdout_color_sinks.h"


namespace gsr::utils
{
	std::string_view get_log_path();
	std::string_view get_database_file_path();
	std::shared_ptr<spdlog::logger> get_or_create_logger(std::string_view loggerName);
}
