#include "utils.h"

#if defined(LOG_TO_CONSOLE)
#define CREATE_LOGGER(loggerName) spdlog::stdout_color_mt(loggerName)
#else
#define CREATE_LOGGER(loggerName) spdlog::basic_logger_mt(loggerName, utils::get_log_path().data())
#endif

namespace gsr::utils
{
	std::string_view get_log_path()
	{
		return "gaser_log.txt";
	}
	
	std::string_view get_database_file_path()
	{
		return "gaser.db";
	}
	
	std::shared_ptr<spdlog::logger> get_or_create_logger(std::string_view loggerName)
	{
		auto logger = spdlog::get(loggerName.data());
		if (logger)
			return logger;
		
		return CREATE_LOGGER(loggerName.data());
	}
}