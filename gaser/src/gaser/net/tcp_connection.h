#pragma once

#include "connection.h"

#include "spdlog/spdlog.h"

#include <memory>

namespace gsr
{
	class tcp_connection final : public connection
	{
		// TODO: use unique id instead
		static inline size_t tcpConnectionId = 0;
		
	public:
		tcp_connection(asio::ip::tcp::socket socket, asio::io_service& service, std::unique_ptr<read_strategy> readStrategy);
		
		~tcp_connection() override
		{
			spdlog::drop(m_Logger->name());
		}
		
		void close(const std::function<void()>& closedCallback) override;
		
		bool is_open() const override
		{
			return m_Socket.is_open();
		}
		
		/*
		 * connects to a remote end with a specified ip and port and calls the connectedCallback if the connection is live
		 */
		void connect(const std::string& ip, unsigned short port, const std::function<void()>& connectedCallback)
		{
			if (is_open())
			{
				// we invoke connect if the socket is closed
				close([this, ip, port, connectedCallback]() {
					connect_impl(ip, port, connectedCallback);
				});
			}
			else
			{
				connect_impl(ip, port, connectedCallback);
			}
		}
		
		void connect_impl(const std::string& ip, unsigned short port, const std::function<void()>& connectedCallback)
		{
			asio::ip::tcp::endpoint endpoint(asio::ip::make_address(ip), port);
			m_Socket = asio::ip::tcp::socket(m_Service);
			m_Socket.async_connect(endpoint, [this, connectedCallback](const std::error_code& ec) {
				if (ec)
				{
					m_Logger->error("Error: " + ec.message());
					return;
				}
				connectedCallback();
			});
		}
		
	private:
		void send_async_impl(const std::function<void()>& doneCallback) override;
		void receive_async_impl(const std::function<void()>& doneCallback) override;
		
	private:
		asio::ip::tcp::socket m_Socket;
		std::shared_ptr<spdlog::logger> m_Logger;
	};
}