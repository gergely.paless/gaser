#pragma once

#include "connection.h"

#include "gaser/utils/utils.h"

#include "spdlog/spdlog.h"

#include <string>

namespace gsr
{
	class udp_connection final : public connection
	{
		// TODO: use unique id instead
		static inline size_t udpConnectionId = 0;
		
	public:
		udp_connection(
				asio::io_service& service,
				std::unique_ptr<read_strategy> readStrategy,
				asio::ip::udp::socket socket,
				asio::ip::udp::endpoint remoteEndpoint
				);
		
		~udp_connection() override
		{
			spdlog::drop(m_Logger->name());
		}
		
		void close(const std::function<void()>& closedCallback) override;
		
		bool is_open() const override
		{
			return m_Socket.is_open();
		}
		
		/*
		 * binds the socket to a specified ip and port
		 */
		void bind(const std::string& bindIP, short bindPort);
		
	private:
		void send_async_impl(const std::function<void()>& doneCallback) override;
		void receive_async_impl(const std::function<void()>& doneCallback) override;
	
	private:
		asio::ip::udp::socket m_Socket;
		asio::ip::udp::endpoint m_RemoteEndpoint;
		std::shared_ptr<spdlog::logger> m_Logger;
	};
}