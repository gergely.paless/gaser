#include "tcp_connection.h"

#include "gaser/utils/utils.h"

#include <iostream>
#include <string>

namespace gsr
{
	tcp_connection::tcp_connection(asio::ip::tcp::socket socket, asio::io_service& service, std::unique_ptr<read_strategy> readStrategy) :
			connection(service, std::move(readStrategy)),
			m_Socket(std::move(socket)),
			m_Logger(utils::get_or_create_logger("tcp_connection" + std::to_string(tcpConnectionId++)))
	{
	
	}
	
	void tcp_connection::close(const std::function<void()>& closedCallback)
	{
		m_Service.post(
				[this, closedCallback]() {
					if (m_Socket.is_open())
					{
						try
						{
							m_Socket.shutdown(asio::socket_base::shutdown_both);
						}
						catch (...) {} // "eat" the exceptions
						m_Socket.close();
					}
					closedCallback();
				}
		);
	}
	
	void tcp_connection::send_async_impl(const std::function<void()>& doneCallback)
	{
		// If this function is called, we know the outgoing message queue must have
		// at least one message to send. So allocate a transmission buffer to hold
		// the message, and issue the work - asio, send these bytes
		auto& packet = m_OutgoingBuffers.front(); // TODO: create function which returns the next packet to send
		asio::async_write(m_Socket, asio::buffer(packet.whole()),
			      [this, doneCallback](const std::error_code& ec, std::size_t bytes_transferred) {
			          if (ec)
			          {
			              // ...asio failed to write the message, we could analyse why but
			              // for now simply assume the connection has died by closing the
			              // socket. When a future attempt to write to this client fails due
			              // to the closed socket, it will be tidied up.
				          m_Logger->error("Read Header Fail. Error: {}", ec.message());
			              m_Socket.close();
			              return;
			          }
			
					  m_Logger->info("Sent {} bytes", std::to_string(bytes_transferred));
			          doneCallback();
			      }
		);
	}
	
	void tcp_connection::receive_async_impl(const std::function<void()>& doneCallback)
	{
		if (m_ReadStrategy->how_many_to_receive() == 0)
		{
			m_ReadStrategy->publish_packet(weak_from_this());
			doneCallback();
			return;
		}
		
		auto buffer = m_ReadStrategy->get_buffer();
		asio::async_read(m_Socket, buffer,
                 [this, doneCallback](const std::error_code& ec, std::size_t bytes_transferred) {
	                 if (ec)
	                 {
		                 // Reading form the client went wrong, most likely a disconnect
		                 // has occurred. Close the socket and let the system tidy it up later.
		                 m_Logger->error("Read Header Fail. Error: " + ec.message());
		                 m_Socket.close();
		                 return;
	                 }
					 
					 m_ReadStrategy->received_bytes(bytes_transferred);
	                 receive_async_impl(doneCallback);
                 }
		);
	}
}


