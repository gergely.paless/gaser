#include "connection.h"

#include <iostream>


namespace gsr
{
	connection::connection(asio::io_service& service, std::unique_ptr<read_strategy> readStrategy) :
			m_Service(service),
			m_ReadStrategy(std::move(readStrategy))
	{
	
	}
	
	void connection::start_receiving()
	{
		receive_async_impl([this]() {
			start_receiving();
		});
	}
	
	void connection::start_sending()
	{
		// we assume that m_QueueMutex is locked
		send_async_impl([&]() {
			send_done_callback();
		});
	}
	
	void connection::send(const packet& packet)
	{
		m_Service.post(
				[this, packet]() {
					// If the queue has a message in it, then we must
					// assume that it is in the process of asynchronously being written.
					// Either way add the message to the queue to be output. If no messages
					// were available to be written, then start the process of writing the
					// message at the front of the queue.
					std::scoped_lock lock(m_OutgoingQueueMutex);
					bool socketIsWritingMessage = !m_OutgoingBuffers.empty();
					m_OutgoingBuffers.emplace_back(packet);
					if (!socketIsWritingMessage)
					{
						start_sending();
					}
				}
		);
	}
	
	void connection::send_done_callback()
	{
		std::scoped_lock lock(m_OutgoingQueueMutex);
		
		// we are done with this message. Remove it from
		// the outgoing message queue
		m_OutgoingBuffers.pop_front();
		
		// If the queue is not empty, there are more messages to send, so
		// make this happen by issuing the task to send the next header.
		if (!m_OutgoingBuffers.empty())
		{
			start_sending();
		}
	}
}