#include "packet.h"


namespace gsr
{
	packet::packet(std::vector <uint8_t> body) :
			m_Body(std::move(body))
	{
	
	}
	
	const packet::buffer_t& packet::body() const
	{
		return m_Body;
	}
	
	std::size_t packet::size() const
	{
		return m_Body.size();
	}
	
	asio::mutable_buffer packet::get_asio_buffer()
	{
		return { m_Body.data(), m_Body.size() };
	}
	
	std::vector<uint8_t> packet::whole()
	{
		std::vector<uint8_t> wholePacket;
		wholePacket.resize(sizeof(decltype(m_Body.size())) + m_Body.size());
		auto size = m_Body.size();
		std::memcpy(wholePacket.data(), &size, sizeof(decltype(size)));
		std::memcpy(wholePacket.data() + sizeof(decltype(size)), m_Body.data(), m_Body.size());
		return wholePacket;
	}
}
