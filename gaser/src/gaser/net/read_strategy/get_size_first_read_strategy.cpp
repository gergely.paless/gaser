#include "get_size_first_read_strategy.h"

namespace gsr
{
	asio::mutable_buffer get_size_first_read_strategy::get_buffer()
	{
		switch (m_State)
		{
			case state::Initial:
				return { &m_SizeBuffer, sizeof(decltype(m_SizeBuffer)) };
			
			case state::SizeWasRead:
				m_BodyBuffer.resize(m_SizeBuffer);
			case state::BodyWasRead:
				return { m_BodyBuffer.data(), m_BodyBuffer.size() };
			
			default:
				throw std::logic_error("unknown state");
		}
	}
	
	size_t get_size_first_read_strategy::how_many_to_receive()
	{
		switch (m_State)
		{
			case state::Initial:
				return sizeof(decltype(m_SizeBuffer));
			case state::SizeWasRead:
				return m_SizeBuffer;
			case state::BodyWasRead:
				return 0;
			default:
				throw std::logic_error("unknown state");
		}
	}
	
	void get_size_first_read_strategy::received_bytes(size_t receivedByteCount)
	{
		switch (m_State)
		{
			case state::Initial:
				m_State = state::SizeWasRead;
				break;
			
			case state::SizeWasRead:
				m_State = state::BodyWasRead;
				break;
			
			case state::BodyWasRead:
				throw std::logic_error("body was already received, please publish the packet");
		}
	}
	
	void get_size_first_read_strategy::publish_packet(const std::weak_ptr<connection>&)
	{
		m_State = state::Initial;
	}
}