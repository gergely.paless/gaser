#pragma once

#include "asio.hpp"

namespace gsr
{
	class read_strategy
	{
	public:
		virtual ~read_strategy() = default;
		virtual asio::mutable_buffer get_buffer() = 0;
		virtual size_t how_many_to_receive() = 0;
		virtual void received_bytes(size_t receivedByteCount) = 0;
		virtual void publish_packet(const std::weak_ptr<class connection>&) = 0;
	};
}
