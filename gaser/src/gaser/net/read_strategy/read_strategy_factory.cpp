#include "read_strategy_factory.h"

namespace gsr
{
	std::unique_ptr<read_strategy> read_strategy_factory::create_read_strategy()
	{
		return std::make_unique<get_size_first_read_strategy>();
	}
}