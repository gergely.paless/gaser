#pragma once

#include "read_strategy.h"
#include "get_size_first_read_strategy.h"

namespace gsr
{
	class read_strategy_factory
	{
	public:
		virtual ~read_strategy_factory() = default;
		
		virtual std::unique_ptr<read_strategy> create_read_strategy();
	};
}
