#pragma once

#include "read_strategy.h"
#include "gaser/net/packet.h"
#include "gaser/utils/utils.h"
#include "gaser/net/tcp_connection.h"

#include "spdlog/spdlog.h"

#include <memory>
#include <utility>
#include <vector>

namespace gsr
{
	class get_size_first_read_strategy : public read_strategy
	{
	private:
		enum class state
		{
			Initial,
			SizeWasRead,
			BodyWasRead,
		};
		
	public:
		asio::mutable_buffer get_buffer() override;
		size_t how_many_to_receive() override;
		void received_bytes(size_t receivedByteCount) override;
		void publish_packet(const std::weak_ptr<connection>&) override;
		
	private:
		state m_State = state::Initial;
		
	protected:
		packet::size_t m_SizeBuffer{};
		packet::buffer_t m_BodyBuffer;
		
	};
}
