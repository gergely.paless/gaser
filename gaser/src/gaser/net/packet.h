#pragma once

#include "asio.hpp"

#include <vector>
#include <cstdint>

namespace gsr
{
	class packet
	{
	public:
		using size_t = uint32_t;
		using buffer_t = std::vector<uint8_t>;
		
		explicit packet(buffer_t body);
		
		const buffer_t& body() const;
		std::size_t size() const;
		asio::mutable_buffer get_asio_buffer();
		
		std::vector<uint8_t> whole();
	
	private:
		buffer_t m_Body;
	};
}