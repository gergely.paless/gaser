#include "udp_connection.h"

namespace gsr
{
	
	udp_connection::udp_connection(
			asio::io_service& service,
			std::unique_ptr<read_strategy> readStrategy,
			asio::ip::udp::socket socket,
			asio::ip::udp::endpoint remoteEndpoint) :
			connection(service, std::move(readStrategy)),
			m_Socket(std::move(socket)),
			m_RemoteEndpoint(std::move(remoteEndpoint)),
			m_Logger(utils::get_or_create_logger("udp_connection" + std::to_string(udpConnectionId++)))
	{
		m_Socket.open(asio::ip::udp::v4());
	}
	
	void udp_connection::close(const std::function<void()>& closedCallback)
	{
		m_Service.post(
				[this, closedCallback]() {
					if (m_Socket.is_open())
					{
						try
						{
							m_Socket.shutdown(asio::socket_base::shutdown_both);
						}
						catch (...) {} // "eat" the exceptions
						m_Socket.close();
					}
					closedCallback();
				}
		);
	}
	
	void udp_connection::bind(const std::string& bindIP, short bindPort)
	{
		//m_Socket.open(asio::ip::udp::v4()); // TODO: ?? kell?
		m_Socket.bind(asio::ip::udp::endpoint(asio::ip::make_address(bindIP), bindPort));
		
		m_Logger->info(
				"Bound to endpoint: " +
				m_Socket.local_endpoint().address().to_string() +
				":" +
				std::to_string(m_Socket.local_endpoint().port())
		);
	}
	
	void udp_connection::send_async_impl(const std::function<void()>& doneCallback)
	{
		// If this function is called, we know the outgoing message queue must have
		// at least one message to send. So allocate a transmission packet to hold
		// the message, and issue the work - asio, send these bytes
		auto& packet = m_OutgoingBuffers.front();
		m_Socket.async_send_to(asio::buffer(packet.whole()), m_RemoteEndpoint,
		                       [this, doneCallback](std::error_code ec, std::size_t bytes_transferred) {
			                       if (ec)
			                       {
				                       // ...asio failed to write the message, we could analyse why but
				                       // for now simply assume the connection has died by closing the
				                       // socket. When a future attempt to write to this client fails due
				                       // to the closed socket, it will be tidied up.
				                       m_Logger->error("Write Header Fail. Error: " + ec.message());
				                       m_Socket.close();
				                       return;
			                       }
			
			                       doneCallback();
		                       }
		);
	}
	
	void udp_connection::receive_async_impl(const std::function<void()>& doneCallback)
	{
		if (m_ReadStrategy->how_many_to_receive() == 0)
		{
			m_ReadStrategy->publish_packet(weak_from_this());
			doneCallback();
			return;
		}
		
		auto buffer = m_ReadStrategy->get_buffer();
		m_Socket.async_receive_from(buffer, m_RemoteEndpoint,
                        [this, doneCallback](std::error_code ec, std::size_t bytes_transferred) {
                            if (ec)
                            {
	                            // Reading form the client went wrong, most likely a disconnect
	                            // has occurred. Close the socket and let the system tidy it up later.
	                            m_Logger->error("Read Header Fail. Error: " + ec.message());
	                            m_Socket.close();
	                            return;
                            }
							
	                        m_ReadStrategy->received_bytes(bytes_transferred);
	                        receive_async_impl(doneCallback);
                        }
		);
		
	}
}