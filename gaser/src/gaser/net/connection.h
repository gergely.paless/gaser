#pragma once

#include "packet.h"
#include "gaser/net/read_strategy/read_strategy.h"

#include "asio.hpp"

#include <functional>
#include <mutex>
#include <deque>
#include <memory>


namespace gsr
{
	class connection : public std::enable_shared_from_this<connection>
	{
	public:
		connection(asio::io_service& service, std::unique_ptr<read_strategy> readStrategy);
		
		connection(const connection&) = delete;
		connection& operator=(const connection&) = delete;
		
		virtual ~connection() = default;
		
		/*
		 * starts listening for packets from the remote end
		 */
		void start_receiving();
		
		/*
		 * closes the connection with the remote end
		 */
		void close() { close([](){}); }
		
		/*
		 * closes the remote connection with the remote end and calls the callback when the connection is not present
		 */
		virtual void close(const std::function<void()>& closedCallback) = 0;
		
		/*
		 * sends a packet to the remote end
		 */
		void send(const packet& packet);
		
		virtual bool is_open() const = 0;
		
	protected:
		// send_impl can block
		//virtual void send_impl() = 0;
		// send_impl must send the packet on another thread, or else there will be deadlock
		virtual void send_async_impl(const std::function<void()>& doneCallback) = 0;
		
		// receive_async_impl must receive on another thread
		virtual void receive_async_impl(const std::function<void()>& doneCallback) = 0;
		
	private:
		void send_done_callback();
		void start_sending();
		
	protected:
		// This queue holds all messages to be sent to the remote side
		// of this connection
		std::deque<packet> m_OutgoingBuffers; // shared_ptr because there may be messages waiting that are the same at two different clients
		std::mutex m_OutgoingQueueMutex;
		
		asio::io_service& m_Service;
		
		std::unique_ptr<read_strategy> m_ReadStrategy;
	};
}
