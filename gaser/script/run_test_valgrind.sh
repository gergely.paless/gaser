#!/bin/bash

# this file should be run from the project root directory

ValgrindArguments="--error-exitcode=1 --leak-check=full --leak-resolution=med --track-origins=yes --vgdb=no"

valgrind $ValgrindArguments ../build/gaser/test/GaserServerTest
GaserServerValgrindTestResult=$?

valgrind $ValgrindArguments ../build/gaser/test/GaserConnectionTest
GaserConnectionValgrindTestResult=$?

if  [ $GaserServerValgrindTestResult == 0 ] &&
    [ $GaserConnectionValgrindTestResult == 0 ]
then
  exit 0
else
  exit 1
fi

