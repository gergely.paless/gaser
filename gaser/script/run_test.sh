#!/bin/bash

# this file should be run from the project root directory

../build/gaser/test/GaserServerTest
GaserServerTestResult=$?

../build/gaser/test/GaserConnectionTest
GaserConnectionTestResult=$?

if  [ $GaserServerTestResult  == 0 ] &&
    [ $GaserConnectionTestResult == 0 ]
then
  exit 0
else
    exit 1
fi
