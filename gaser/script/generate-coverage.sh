#!/bin/bash

# this file should be run from the gaser directory

CAPTURE_DIR=./gaser/src/CMakeFiles/gaser-testing.dir/gaser
COVERAGE_INFO_DIR=coverage-info
COVERAGE_INFO_FILE=coverage.info

GASR_ROOT_DIR=$PWD

cd ../build

gcov $CAPTURE_DIR/utils.cpp.gcno || exit
mkdir -p $COVERAGE_INFO_DIR
lcov --capture --directory $CAPTURE_DIR --output-file=$COVERAGE_INFO_DIR/$COVERAGE_INFO_FILE || exit
lcov --remove $COVERAGE_INFO_DIR/$COVERAGE_INFO_FILE '/usr/include/*' '/usr/lib/*' "$GASR_ROOT_DIR/deps/*" -o $COVERAGE_INFO_DIR/$COVERAGE_INFO_FILE
genhtml $COVERAGE_INFO_DIR/$COVERAGE_INFO_FILE --output-directory=$COVERAGE_INFO_DIR/gaser || exit
#open $COVERAGE_INFO_DIR/index.html || exit
