cmake_minimum_required(VERSION 3.16)


file(GLOB_RECURSE SQLITE_HEADERS *.h *.hpp)
file(GLOB_RECURSE SQLITE_SOURCES *.cpp *.c)
add_library(sqlite STATIC ${SQLITE_HEADERS} ${SQLITE_SOURCES})

set_target_properties(sqlite PROPERTIES LINKER_LANGUAGE C)

target_include_directories(sqlite PUBLIC include)
