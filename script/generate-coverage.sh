#!/bin/bash

# this file should be run from the project root directory

PONG_ROOT_DIR=$PWD
CAPTURE_DIR=./src/CMakeFiles/pong-server-testing.dir
COVERAGE_INFO_DIR=coverage-info
COVERAGE_INFO_FILE=coverage.info

cd build

gcov $CAPTURE_DIR/PongServer.cpp.gcno || exit
mkdir -p $COVERAGE_INFO_DIR
lcov --capture --directory $CAPTURE_DIR --output-file=$COVERAGE_INFO_DIR/$COVERAGE_INFO_FILE || exit
lcov --remove $COVERAGE_INFO_DIR/$COVERAGE_INFO_FILE '/usr/include/*' '/usr/lib/*' "$PONG_ROOT_DIR/gaser/*" -o $COVERAGE_INFO_DIR/$COVERAGE_INFO_FILE
genhtml $COVERAGE_INFO_DIR/$COVERAGE_INFO_FILE --output-directory=$COVERAGE_INFO_DIR/pong-server || exit
#open $COVERAGE_INFO_DIR/index.html || exit
