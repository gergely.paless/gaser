#!/bin/bash

# this file should be run from the project root directory

./build/test/PongServerTest
PongServerTestResult=$?

if [ $PongServerTestResult == 0 ]
then
  exit 0
else
    exit 1
fi
