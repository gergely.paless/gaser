#!/bin/bash

# this file should be run from the project root directory

ValgrindArguments="--error-exitcode=1 --leak-check=full --leak-resolution=med --track-origins=yes --vgdb=no"

valgrind $ValgrindArguments ./build/test/PongServerTest
PongServerValgrindTestResult=$?

if [ $PongServerValgrindTestResult == 0 ]
then
  exit 0
else
  exit 1
fi

