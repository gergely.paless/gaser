#pragma once

#include "gaser/core/server/connection_handler.h"
#include "gaser/net/tcp_connection.h"

class ConnectionHandler : public gsr::connection_handler
{
public:
	ConnectionHandler();
	
	bool handle_connection_error(const std::error_code& ec, const asio::ip::tcp::socket& newConnection) override;
	void on_incoming_connection(const std::shared_ptr<gsr::tcp_connection>& socket) override;
	
	const std::vector<std::shared_ptr<gsr::tcp_connection>>& connections() const override
	{
		return m_Connections;
	}
	
private:
	std::vector<std::shared_ptr<gsr::tcp_connection>> m_Connections;
	std::mutex m_ConnectionMutex;
	
	std::shared_ptr<spdlog::logger> m_Logger;
};