#pragma once

#include "gaser/core/client/client_handler.h"

class ClientHandler : public gsr::client_handler
{
public:
	ClientHandler();
	
	bool on_client_connect(std::shared_ptr<gsr::tcp_connection> incomingConnection) override;
	void on_client_disconnect(std::shared_ptr<gsr::tcp_connection> connection) override;
	void packet_received(std::shared_ptr<gsr::connection> connection, std::shared_ptr<gsr::packet> packet) override;
	
private:
	std::shared_ptr<spdlog::logger> m_Logger;
};

