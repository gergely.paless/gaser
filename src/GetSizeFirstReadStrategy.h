#pragma once

#include "gaser/net/read_strategy/get_size_first_read_strategy.h"
#include "gaser/net/tcp_connection.h"

class PongServer;

class GetSizeFirstReadStrategy : public gsr::get_size_first_read_strategy
{
	static inline size_t uniqueId = 0;
	
public:
	explicit GetSizeFirstReadStrategy(PongServer&);
	
	void publish_packet(const std::weak_ptr<gsr::connection>&) override;
	
private:
	PongServer& m_PongServer;
	std::shared_ptr<spdlog::logger> m_Logger;
};

