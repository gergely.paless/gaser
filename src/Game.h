#pragma once

#include "PacketId.h"

#include "gaser/match-maker/game.h"

#include <memory>

class Game : public gsr::game
{
public:
	explicit Game(const std::shared_ptr<gsr::connection>& player1, const std::shared_ptr<gsr::connection>& player2) :
		gsr::game({ player1, player2 })
	{
		std::vector<std::shared_ptr<gsr::connection>> players = { player1, player2 };
		for (auto& connection : players)
		{
			gsr::packet::buffer_t responseBuffer;
			responseBuffer.resize(sizeof(gsr::packet::size_t) + sizeof(PacketId) + sizeof(bool));
			gsr::packet::size_t responsePacketSize = sizeof(PacketId) + sizeof(bool);
			PacketId gameCreatedId = PacketId::GAME_CREATED;
			bool isFirstPlayer = connection == player1;
			std::memcpy(responseBuffer.data(), &responsePacketSize, sizeof(decltype(responsePacketSize)));
			std::memcpy(responseBuffer.data() + sizeof(decltype(responsePacketSize)), &gameCreatedId, sizeof(PacketId));
			std::memcpy(responseBuffer.data() + sizeof(decltype(responsePacketSize)) + sizeof(PacketId), &isFirstPlayer, sizeof(bool));
			gsr::packet gameCreatedPacket(responseBuffer);
			connection->send(gameCreatedPacket);
		}
	}
	
	void update() override
	{
	
	}
	
	std::shared_ptr<gsr::connection> GetOpponent(const std::shared_ptr<gsr::connection>& connection) const
	{
		return *m_ConnectionsInGame.begin() == connection ? *std::next(m_ConnectionsInGame.begin()) : *m_ConnectionsInGame.begin();
	}
	
	bool IsInGame(const std::shared_ptr<gsr::connection>& connection) const
	{
		return m_ConnectionsInGame.count(connection) > 0;
	}
	
};