#include "PongServer.h"

#include "ConnectionHandler.h"
#include "ClientHandler.h"
#include "ReadStrategyFactory.h"

#include "gaser/utils/utils.h"
#include "gaser/net/read_strategy/get_size_first_read_strategy.h"
#include "gaser/net/packet.h"

#include "spdlog/sinks/basic_file_sink.h"
#include "PacketId.h"
#include "PacketProcessor.h"

#include <string_view>

static std::string_view loggerName = "SERVER";

PongServer::PongServer(uint16_t port):
		gsr::server(
				port,
				std::make_unique<ConnectionHandler>(),
				std::make_unique<ClientHandler>(),
				std::make_unique<ReadStrategyFactory>(*this)
				),
		m_Logger(gsr::utils::get_or_create_logger(loggerName.data()))
{
	
}

PongServer::~PongServer()
{
	spdlog::drop(loggerName.data());
}

void PongServer::start()
{
	server::start();
	m_Logger->info("Started on port: {}", port());
}

void PongServer::stop()
{
	server::stop();
	cvNoPacketToProcess.notify_all();
	m_Logger->info("Stopped!");
}

void PongServer::ProcessPackets(size_t maxNumberOfPackets)
{
	std::unique_lock<std::mutex> ul(m_IncomingQueueMutex);
	while (m_IncomingPacketQueue.empty() && m_IsRunning)
		cvNoPacketToProcess.wait(ul);
	
	if (!m_IsRunning)
		return;
	
	size_t packetCount = 0;
	while (packetCount < maxNumberOfPackets && !m_IncomingPacketQueue.empty() && m_IsRunning)
	{
		PostPacketTaskToThreadPool(m_IncomingPacketQueue.front());
		m_IncomingPacketQueue.pop_front();
		++packetCount;
	}
}

void PongServer::OnIncomingPacket(const std::weak_ptr<gsr::connection>& connection, gsr::packet packet)
{
	std::scoped_lock<std::mutex> lock(m_IncomingQueueMutex);
	m_IncomingPacketQueue.emplace_back(std::make_pair(connection, std::move(packet)));
	cvNoPacketToProcess.notify_all();
}

void PongServer::PostPacketTaskToThreadPool(const std::pair<std::weak_ptr<gsr::connection>, gsr::packet>& ownedPacket)
{
	/*std::stringstream ss;
	ss << "packet received: " << std::hex << ownedPacket.second.body().data();
	m_Logger->info(ss.str());*/
	
	m_Service.post([ownedPacket, this](){
		m_PacketProcessor.Process(ownedPacket);
	});
}


