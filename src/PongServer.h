#pragma once

#include "gaser/core/server/server.h"
#include "gaser/net/tcp_connection.h"

#include "asio.hpp"
#include "spdlog/spdlog.h"
#include "PacketProcessor.h"

#include <memory>
#include <mutex>
#include <sstream>

class PongServer : public gsr::server
{
public:
	explicit PongServer(uint16_t port);
	~PongServer() override;
	
	void start() override;
	void stop() override;
	
	void ProcessPackets(size_t maxNumberOfPackets = -1);
	void OnIncomingPacket(const std::weak_ptr<gsr::connection>& connection, gsr::packet packet);
	
private:
	void PostPacketTaskToThreadPool(const std::pair<std::weak_ptr<gsr::connection>, gsr::packet>& packet);
	
private:
	std::deque<std::pair<std::weak_ptr<gsr::connection>, gsr::packet>> m_IncomingPacketQueue;
	std::mutex m_IncomingQueueMutex;
	std::condition_variable cvNoPacketToProcess;
	
	PacketProcessor m_PacketProcessor;
	
	std::shared_ptr<spdlog::logger> m_Logger;
};
