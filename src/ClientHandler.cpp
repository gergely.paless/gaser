#include "ClientHandler.h"

#include "gaser/utils/utils.h"

static std::string_view loggerName = "ClientHandler";

ClientHandler::ClientHandler() :
		m_Logger(gsr::utils::get_or_create_logger(loggerName.data()))
{

}

bool ClientHandler::on_client_connect(std::shared_ptr<gsr::tcp_connection> incomingConnection)
{
	return client_handler::on_client_connect(incomingConnection);
}

void ClientHandler::on_client_disconnect(std::shared_ptr<gsr::tcp_connection> connection)
{
	client_handler::on_client_disconnect(connection);
}

void ClientHandler::packet_received(std::shared_ptr<gsr::connection> connection, std::shared_ptr<gsr::packet> packet)
{
	client_handler::packet_received(connection, packet);
}
