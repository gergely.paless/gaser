#include "GetSizeFirstReadStrategy.h"

#include "PongServer.h"

static std::string_view loggerName = "GetSizeFirstReadStrategy";

GetSizeFirstReadStrategy::GetSizeFirstReadStrategy(PongServer& pongServer) :
	m_PongServer(pongServer),
	m_Logger(gsr::utils::get_or_create_logger(loggerName.data() + std::to_string(uniqueId++)))
{

}

void GetSizeFirstReadStrategy::publish_packet(const std::weak_ptr<gsr::connection>& connection)
{
	gsr::get_size_first_read_strategy::publish_packet(connection);
	m_PongServer.OnIncomingPacket(connection, gsr::packet(std::move(m_BodyBuffer)));
}
