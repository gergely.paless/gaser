#pragma once

#include "gaser/match-maker/match_maker.h"
#include "Game.h"

#include <chrono>
#include <random>

class MatchMaker : public gsr::match_maker
{
public:
	MatchMaker() : m_Generator(m_RandomDevice())
	{
	
	}
	
	Game& GetGameOf(const std::shared_ptr<gsr::connection>& connection)
	{
		std::scoped_lock<std::mutex> lock(m_GamesMutex);
		for (const auto& game : m_Games)
		{
			if (game->IsInGame(connection))
				return *game;
		}
		throw std::invalid_argument("connection has no game");
	}
	
protected:
	void try_to_create_game() override
	{
		using namespace std::chrono_literals;
		if (std::chrono::high_resolution_clock::now() - m_LastTry < 1s)
			return;
		
		if (m_QueuedConnections.size() >= 2)
		{
			std::shuffle(m_QueuedConnections.begin(), m_QueuedConnections.end(), m_Generator);
			auto firstPlayer = m_QueuedConnections.erase(std::prev(m_QueuedConnections.end()));
			auto secondPlayer = m_QueuedConnections.erase(std::prev(m_QueuedConnections.end()));
			std::scoped_lock<std::mutex> gamesLock(m_GamesMutex);
			m_Games.emplace_back(std::make_unique<Game>(*firstPlayer, *secondPlayer));
		}
		
		m_LastTry = std::chrono::high_resolution_clock::now();
	}
	
private:
	std::vector<std::unique_ptr<Game>> m_Games;
	std::mutex m_GamesMutex;
	
	std::chrono::time_point<std::chrono::high_resolution_clock> m_LastTry;
	
	std::random_device m_RandomDevice;
	std::mt19937 m_Generator;
	
};