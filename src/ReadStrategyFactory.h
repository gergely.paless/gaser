#pragma once

#include "gaser/net/read_strategy/read_strategy_factory.h"

#include "spdlog/logger.h"

#include <memory>

class PongServer;

class ReadStrategyFactory : public gsr::read_strategy_factory
{
public:
	explicit ReadStrategyFactory(PongServer&);
	
	std::unique_ptr<gsr::read_strategy> create_read_strategy() override;
	
private:
	PongServer& m_PongServer;
	std::shared_ptr<spdlog::logger> m_Logger{};
};