#pragma once

#include <cstdlib>

enum class PacketId : std::size_t
{
	UNDEFINED = 0,
	PING,
	PINGED,
	LOGIN,
	LOGGED_IN,
	REGISTER,
	REGISTERED,
	GAME_CREATED,
	GAME_UPDATE,
	ENUM_SIZE_
};
