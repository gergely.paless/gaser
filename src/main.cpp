#include "PongServer.h"

#include "fmt/format.h"
#include "spdlog/spdlog.h"
#include "spdlog/sinks/stdout_color_sinks.h"

#include <iostream>

static void InitLogger()
{
	spdlog::set_level(spdlog::level::info);
	spdlog::set_pattern("[%H:%M:%S] [t%t] [%n] %v");
}

int main(int argc, const char** argv)
{
	InitLogger();
	
	PongServer pongServer(3333);
	std::thread serverThread = std::thread([&] () {
		try
		{
			pongServer.start();
			while (pongServer.is_running())
				pongServer.ProcessPackets();
		}
		catch (std::exception& e)
		{
			// Something prohibited the server from listening
			spdlog::error("Exception: {}", e.what());
			exit(1);
		}
	});
	
	std::cin.get();
	pongServer.stop();
	serverThread.join();
	
	return 0;
}
