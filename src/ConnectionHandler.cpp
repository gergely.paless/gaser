#include "ConnectionHandler.h"
#include "gaser/utils/utils.h"

static std::string_view loggerName = "ConnectionHandler";

ConnectionHandler::ConnectionHandler() :
		m_Logger(gsr::utils::get_or_create_logger(loggerName.data()))
{

}

bool ConnectionHandler::handle_connection_error(const std::error_code& ec, const asio::ip::tcp::socket& newConnection)
{
	if (ec)
	{
		m_Logger->error("New Connection Error: {}", ec.message());
	}
	else
	{
		m_Logger->info("Client connected!");
	}
	return !ec;
}

void ConnectionHandler::on_incoming_connection(const std::shared_ptr<gsr::tcp_connection>& socket)
{
	socket->start_receiving();
	std::scoped_lock<std::mutex> lock(m_ConnectionMutex);
	m_Connections.push_back(socket);
}
