#pragma once

#include "PacketId.h"

#include "gaser/net/packet.h"
#include "gaser/net/connection.h"
#include "gaser/utils/utils.h"
#include "gaser/core/client/client_manager.h"

#include "spdlog/logger.h"
#include "MatchMaker.h"

#include <memory>

class PacketProcessor
{
public:
	explicit PacketProcessor() :
			m_Logger(gsr::utils::get_or_create_logger("PacketProcessor"))
	{
		m_MatchMakerThread = std::thread([this]() { m_MatchMaker.run(); });
	}
	
	~PacketProcessor()
	{
		m_MatchMaker.stop();
		m_MatchMakerThread.join();
	}
	
	void Process(const std::pair<std::weak_ptr<gsr::connection>, gsr::packet>& ownedPacket)
	{
		auto body = ownedPacket.second.body();
		
		if (body.empty())
			return;
		
		switch (PacketId{*body.data()})
		{
			case PacketId::PING:
				SendPing(ownedPacket);
				break;
			case PacketId::LOGIN:
				LoginClient(ownedPacket);
				break;
			case PacketId::REGISTER:
				RegisterClient(ownedPacket);
				break;
				
			case PacketId::GAME_UPDATE:
			{
				try
				{
					auto senderPlayer = ownedPacket.first.lock();
					auto game = m_MatchMaker.GetGameOf(senderPlayer);
					auto otherPlayer = game.GetOpponent(senderPlayer);
					
					gsr::packet::buffer_t buffer;
					buffer.resize(sizeof(gsr::packet::size_t) + ownedPacket.second.size());
					gsr::packet::size_t packetSize = ownedPacket.second.size();
					std::memcpy(buffer.data(), &packetSize, sizeof(decltype(packetSize)));
					std::memcpy(buffer.data() + sizeof(decltype(packetSize)), ownedPacket.second.body().data(), ownedPacket.second.size());
					gsr::packet packetToSend(buffer);
					otherPlayer->send(packetToSend);
				}
				catch (const std::invalid_argument& ex)
				{
					m_Logger->error(ex.what());
					return;
				}
				break;
			}
			
			default:
				m_Logger->warn("Unknown PacketId, ignoring this packet");
				return;
		}
	}
	
private:
	void SendPing(const std::pair<std::weak_ptr<gsr::connection>, gsr::packet>& ownedPacket)
	{
		gsr::packet::buffer_t buffer;
		buffer.resize(sizeof(gsr::packet::size_t) + sizeof(PacketId));
		gsr::packet::size_t packetSize = sizeof(PacketId);
		PacketId id = PacketId::PING;
		std::memcpy(buffer.data(), &packetSize, sizeof(decltype(packetSize)));
		std::memcpy(buffer.data() + sizeof(decltype(packetSize)), &id, sizeof(PacketId));
		gsr::packet packetToSend(buffer);
		ownedPacket.first.lock()->send(packetToSend);
	}
	
	void LoginClient(const std::pair<std::weak_ptr<gsr::connection>, gsr::packet>& ownedPacket)
	{
		gsr::client_manager clientManager;
		std::string username = reinterpret_cast<const char*>(ownedPacket.second.body().data() + sizeof(PacketId));
		std::string password = reinterpret_cast<const char*>(ownedPacket.second.body().data() + sizeof(PacketId) + username.size() + 1);
		try
		{
			auto user = clientManager.login_client(username, password);
			m_Logger->info("user {} just logged in", user.username());
			
			// send LOGGED_IN message
			gsr::packet::buffer_t buffer;
			buffer.resize(sizeof(gsr::packet::size_t) + sizeof(PacketId));
			gsr::packet::size_t packetSize = sizeof(PacketId);
			PacketId id = PacketId::LOGGED_IN;
			std::memcpy(buffer.data(), &packetSize, sizeof(decltype(packetSize)));
			std::memcpy(buffer.data() + sizeof(decltype(packetSize)), &id, sizeof(PacketId));
			gsr::packet loggedInPacket(buffer);
			auto client = ownedPacket.first.lock();
			client->send(loggedInPacket);
			m_MatchMaker.queue_client(client);
		}
		catch (const std::logic_error& err)
		{
			m_Logger->error(std::string("logging in client was unsuccessful. Error occurred: ") + err.what());
		}
	}
	
	void RegisterClient(const std::pair<std::weak_ptr<gsr::connection>, gsr::packet>& ownedPacket)
	{
		gsr::client_manager clientManager;
		std::string username = reinterpret_cast<const char*>(ownedPacket.second.body().data() + sizeof(PacketId));
		std::string password = reinterpret_cast<const char*>(ownedPacket.second.body().data() + sizeof(PacketId) + username.size() + 1);
		try
		{
			clientManager.register_client(username, password);
			m_Logger->info("user {} is now registered", username);
			
			// send REGISTERED message
			gsr::packet::buffer_t buffer;
			buffer.resize(sizeof(gsr::packet::size_t) + sizeof(PacketId));
			gsr::packet::size_t packetSize = sizeof(PacketId);
			PacketId id = PacketId::REGISTERED;
			std::memcpy(buffer.data(), &packetSize, sizeof(decltype(packetSize)));
			std::memcpy(buffer.data() + sizeof(decltype(packetSize)), &id, sizeof(PacketId));
			gsr::packet packetToSend(buffer);
			ownedPacket.first.lock()->send(packetToSend);
		}
		catch (std::logic_error& err)
		{
			m_Logger->error(std::string("registering client was unsuccessful. Error occurred: ") + err.what());
		}
	}
	
private:
	MatchMaker m_MatchMaker;
	std::thread m_MatchMakerThread;
	
	std::shared_ptr<spdlog::logger> m_Logger;
};
