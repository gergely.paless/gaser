#include "ReadStrategyFactory.h"

#include "GetSizeFirstReadStrategy.h"

static std::string_view loggerName = "ReadStrategyFactory";

ReadStrategyFactory::ReadStrategyFactory(PongServer& pongServer) :
	m_PongServer(pongServer),
	m_Logger(gsr::utils::get_or_create_logger(loggerName.data()))
{

}

std::unique_ptr<gsr::read_strategy> ReadStrategyFactory::create_read_strategy()
{
	return std::make_unique<GetSizeFirstReadStrategy>(m_PongServer);
}
